/*****************************/
/* /common/obtenerParametros	*/
/*****************************/
export interface IConfigStatic {
    UrlAPI: string;
    UrlFireBase: string;
    timeOut: number;
  }
