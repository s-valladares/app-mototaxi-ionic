import { IUsuario, Usuario } from '../Usuarios/usuario.interface';

export interface IMensajesRs {
    size: number;
    rows: IMensajes[];
}

export interface IMensajes {
    id: string;
    usuarioEnvia: IUsuario;
    usuarioRecibe: IUsuario;
    type: string;
    mensaje?: string;
    createdAt?: string;
    updatedAt?: string;
}

export class Mensajes {
    static empty() {
        return {
            id: '',
            usuarioEnvia: Usuario.empty(),
            usuarioRecibe: Usuario.empty(),
            mensaje: '',
            type: 'send',
            createdAt: '',
            updatedAt: ''
        };
    }
}
