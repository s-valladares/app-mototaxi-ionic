import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Client } from '@stomp/stompjs';
import { map } from 'rxjs/operators';
import { ConfigService } from '../config/config.service';
import { getHeaders } from '../misc/Headers';
import { IMensajes } from './mensaje.interface';

@Injectable({
  providedIn: 'root'
})
export class MensajeService {

 /** Nombre de recurso ha obtener en la API */
 private mService = '/notification/token';
 /** Url obtenida del servicio de configuracion */
 private mUrl = this.configService.urlLocal;
 private mUrlBase = this.configService.urlWebSocket

 private client: Client;

 constructor(
   private httpClient: HttpClient,
   private configService: ConfigService
 ) { }


  newMensaje(mensaje: IMensajes) {
    const JsonUsuario = JSON.stringify(mensaje);
    return this.httpClient.post(this.mUrlBase + this.mService, JsonUsuario, {
      headers: getHeaders()
    }).pipe(
      map((data: any) => {
        return data.RES;
      })).toPromise();
  }

  newMensajeTest(mensaje: any) {
    const JsonUsuario = JSON.stringify(mensaje);
    return this.httpClient.post(this.mUrlBase + this.mService, JsonUsuario, {
      headers: getHeaders()
    }).pipe(
      map((data: any) => {
        return data.RES;
      })).toPromise();
  }

}
