import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UViajesPageRoutingModule } from './u-viajes-routing.module';

import { UViajesPage } from './u-viajes.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UViajesPageRoutingModule
  ],
  declarations: [UViajesPage]
})
export class UViajesPageModule {}
