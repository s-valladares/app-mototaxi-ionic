import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UViajesPage } from './u-viajes.page';

describe('UViajesPage', () => {
  let component: UViajesPage;
  let fixture: ComponentFixture<UViajesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UViajesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UViajesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
