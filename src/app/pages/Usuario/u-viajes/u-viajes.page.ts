import { Component, OnInit } from '@angular/core';
import { constantesId } from 'src/app/services/misc/enums';
import { EncryptAndStorage } from 'src/app/services/misc/storage';
import { UsuarioService } from 'src/app/services/services.index';

@Component({
  selector: 'app-u-viajes',
  templateUrl: './u-viajes.page.html',
  styleUrls: ['./u-viajes.page.scss'],
})
export class UViajesPage implements OnInit {

  viajes: any[];
  usuarioId: string;
  urlAvatar: string;

  constructor(
    private service: UsuarioService
  ) {
    this.viajes = [];
    this.usuarioId = '';
   }

  ngOnInit() {
    this.usuarioId = EncryptAndStorage.getEncryptStorage(constantesId.usuarioId);
    this.urlAvatar = '../../../../assets/icon/usuario.svg';
    this.getAllViajes();
  }

  getAllViajes() {

    this.service.getAllViajesPilotoId(this.usuarioId)
      .then(data => {
        this.viajes = data.rows;
        console.log(this.viajes);
      })
      .catch(error => {
        console.log(error);
      });
  }

  repetirViaje(item){

  }


}
