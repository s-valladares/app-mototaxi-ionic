import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UViajesPage } from './u-viajes.page';

const routes: Routes = [
  {
    path: '',
    component: UViajesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UViajesPageRoutingModule {}
