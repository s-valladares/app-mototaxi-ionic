import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, Platform, LoadingController } from '@ionic/angular';
import { SettingsComponent } from '../settings/settings/settings.component';
import { Router } from '@angular/router';
import { PilotosService, UsuarioService } from 'src/app/services/services.index';
import { LStorage, EncryptAndStorage } from 'src/app/services/misc/storage';
import { constantesId } from 'src/app/services/misc/enums';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
import { error } from 'protractor';
import { IUsuario, Usuario } from 'src/app/services/interfaces.index';
import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { ConfigService } from 'src/app/services/config/config.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  isPiloto: boolean;
  usuarioId: string;
  usuario: IUsuario;
  private client: Client;
  private mUrl = this.configService.urlWebSocket;

  pages = [
    {
      title: 'Mi Perfil',
      url: '/home/perfil',
      icon: 'person',
      mostrar: 'ok'
    },
    {
      title: 'Mis viajes',
      url: '/home/u-viajes',
      icon: 'car',
      mostrar: 'ok'
    },
    {
      title: 'Salir',
      icon: 'ios-exit',
      mostrar: 'ok'
    },
    {
      title: 'Configurar',
      icon: 'settings',
      mostrar: 'ok'
    }

  ];
  constructor(
    public modalController: ModalController,
    public alertController: AlertController,
    public router: Router,
    public servicePiloto: PilotosService,
    public serviceUsuario: UsuarioService,
    public platform: Platform,
    private configService: ConfigService,
    private loadingController: LoadingController


  ) {
    this.usuario = Usuario.empty();
    
   }

  ngOnInit() {
    this.usuarioId = EncryptAndStorage.getEncryptStorage(constantesId.usuarioId);
    this.isPiloto = this.serviceUsuario.isPiloto();
    this.getTokenFCM(); 
    //this.configWS();
  }

  getTokenFCM(){
    
    this.usuario.id = this.usuarioId;
    
    this.platform.ready().then(() => {

      FCM.getToken().then(token => {
        this.usuario.tokenFCM = token;
        this.updateTokenFCM(this.usuario);
        console.log(token);
      }).catch(error => {
       
        console.log(error);
      });

      
      FCM.onTokenRefresh().subscribe(token=> {
        this.usuario.tokenFCM = token;
        this.updateTokenFCM(this.usuario);
        console.log(token);
      }, error=> {
        console.log(error);
      });

      FCM.onNotification().subscribe(data=> {
        console.log(data);
        if (data.wasTapped){
          console.log('Estamos en segundo plano');
        } else {
          console.log('Estamos en primer plano ' + JSON.stringify(data));
        }
      });



    }, error => { console.log(error) });


  }

  updateTokenFCM(usuario){
    this.serviceUsuario.updateTokenFcm(usuario)
    .then(data => {


    }).catch(error=>{
      console.log(error);
    });

  }

  configWS() {
    //this.presentLoading();
    this.client = new Client();
    this.client.webSocketFactory = () => {
      return new SockJS(this.mUrl + '/mototaxis');
    };

    this.client.onConnect = (frame) => {
      if (this.client.connected) {
        //this.dismissLoading();
        console.log('Conectado: ' + this.client.connected);

        this.client.subscribe('/ubicaciones/mensajes', e => {
          this.alertError();
        });

        
      }

    };

    this.client.onDisconnect = (frame) => {
      console.log('Desconectado');
    };

    this.client.activate();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      spinner: 'lines-small',
      cssClass: 'spinner-loading',
      message: 'Conectando...'
    });
    await loading.present();
  }

  private dismissLoading() {
    this.loadingController.dismiss();
  }

  accion(p) {
    if (p.title === 'Configurar') {
      this.presentModal();
    }
    if (p.title === 'Salir') {
      this.logout();
    }
    
  }

  async logout() {
    const alert = await this.alertController.create({
      header: '¿Cerrar sesión?',
      message: '<strong>¿Está seguro de cerrar sesión?</strong>',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {

          }
        }, {
          text: 'Salir',
          handler: () => {
            this.serviceUsuario.logOut();
            this.router.navigate(['/login']);
          }
        }
      ]
    });

    await alert.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SettingsComponent,
      cssClass: 'my-custom-modal-css',
      componentProps: {
        tipoModal: 'config'
      }
    });
    return await modal.present();
  }

  alertError() {
    Swal.fire({
      title: 'Solicitud de viaje',
      text: "Salvador Valladares solicita un viaje",
      icon: 'info',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: 'primary',
      confirmButtonText: 'Aceptar'
    }).then((result) => {
      if (result.isDismissed) {
        Swal.fire(
          'Rechazada',
          'Rechazaste la solicitud',
          'success'
        )
      }
    })
  }


}
