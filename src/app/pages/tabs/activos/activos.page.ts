import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUbicacion, Ubicacion } from 'src/app/services/ubicacion/ubicacion.interface';
import { Client } from '@stomp/stompjs';
import * as SockJS from 'sockjs-client';
import { PilotosService } from 'src/app/services/Pilotos/pilotos.service';
import { IUbicaciones, Ubicaciones } from 'src/app/services/Ubicaciones/ubicaciones.interface';
import { IUsuario, Usuario } from 'src/app/services/Usuarios/usuario.interface';
import { IPersonas, Personas } from 'src/app/services/Personas/personas.interface';
import { IPilotos, Pilotos } from 'src/app/services/Pilotos/pilotos.interface';
import { EncryptAndStorage } from 'src/app/services/misc/storage';
import { constantesId } from 'src/app/services/misc/enums';
import { LoadingController, ModalController, AlertController } from '@ionic/angular';
import { ConfigService } from 'src/app/services/config/config.service';
import { ChatComponent } from '../mensajes/chat/chat.component';
import Swal from 'sweetalert2'
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { PerfilComponent } from '../../Pilotos/perfil/perfil.component';

@Component({
  selector: 'app-activos',
  templateUrl: './activos.page.html',
  styleUrls: ['./activos.page.scss'],
})
export class ActivosPage implements OnInit, OnDestroy {

  private ubicaciones: IUbicaciones;
  private ubicacion: IUbicaciones;
  private myLocation: IUbicaciones;
  private usuario: IUsuario;
  private persona: IPersonas;
  pilotos: any[];
  pilotosMostrar: any[];
  private client: Client;
  idUsuario: string;

  urlAvatar: string;
  mensajeMostrarLista: string;

  private mUrl = this.configService.urlWebSocket;

  constructor(
    private service: PilotosService,
    private loadingController: LoadingController,
    private configService: ConfigService,
    public modalController: ModalController,
    public alertController: AlertController,
    private geolocation: Geolocation,
  ) {
    this.ubicaciones = Ubicaciones.empty();
    this.usuario = Usuario.empty();
    this.persona = Personas.empty();
    this.ubicacion = Ubicaciones.empty();
    this.myLocation = Ubicaciones.empty();
    this.pilotos = [];
    this.pilotosMostrar = [];
    this.idUsuario = '';
    this.mensajeMostrarLista = '';
  }

  ngOnInit() {

    this.urlAvatar = '../../../../assets/icon/usuario.svg';
    //this.getAllPilotos();
    this.getAllPilotosTemporal(); // solo para pruebas
    this.configWS();
    this.activateWS();
    this.getLocation();
    // this.getAllActivos();
  }

  segmentChanged(ev: any) {
    console.log('Segmento activo: ', ev.detail.value);
    if (ev.detail.value == 'cercanos') {
      this.verCercanos();
    } else if (ev.detail.value == 'todos') {
      this.getAllPilotosTemporal();
    } else if (ev.detail.value == 'prferidos') {
      this.pilotosMostrar = [];
      this.mensajeMostrarLista = 'No hay agregados a favoritos';
    }
  }

  /*

    getAllActivos() {
      this.firestoreService.getAllPilotos()
        .subscribe((ubicaciones) => {

          this.ubicaciones = [];

          ubicaciones
            .forEach((datos: any) => {

              this.ubicaciones.push(datos.payload.doc.data());

              try {

           } catch (error) {
                alert(error);
              }

            });
          console.log(this.ubicaciones);

        }, error => alert(error));

    }
  */

 getAllPilotosTemporal() {

  this.service.getAllPilotosTodos()
    .then(data => {
     
      this.pilotos = data.rows;
      this.pilotosMostrar = [];

      this.pilotos = this.pilotos
        .filter(p =>
          this.calculateDistance(p.lng, this.myLocation.longitud, p.lat, this.myLocation.latitud) < 1000000000000
        )

      this.pilotos.forEach(p => {
        p.distancia = this.calculateDistance(p.lng, this.myLocation.longitud, p.lat, this.myLocation.latitud);
        this.pilotosMostrar.push(p);
      });

      if(this.pilotosMostrar.length == 0) {
        this.mensajeMostrarLista = 'No hay mototaxis disponibles'
      } else {
        this.pilotosMostrar.sort(function(a, b){
          return (a.distancia - b.distancia)
        });
      }

      
      
      console.log(this.pilotos);
    })
    .catch(error => {
      console.log(error);
      this.dismissLoading();
    });
}

  getAllPilotos() {

    this.service.getAllPilotos()
      .then(data => {
        this.pilotos = data.rows;
        this.pilotosMostrar = [];

        this.pilotos = this.pilotos
          .filter(p =>
            this.calculateDistance(p.lng, this.myLocation.longitud, p.lat, this.myLocation.latitud) < 3500
          )

        this.pilotos.forEach(p => {
          p.distancia = this.calculateDistance(p.lng, this.myLocation.longitud, p.lat, this.myLocation.latitud);
          this.pilotosMostrar.push(p);
        });

        if(this.pilotosMostrar.length == 0) {
          this.mensajeMostrarLista = 'No hay mototaxis disponibles'
        } else {
          this.pilotosMostrar.sort(function(a, b){
            return (a.distancia - b.distancia)
          });
        }

        
        
        console.log(this.pilotos);
      })
      .catch(error => {
        console.log(error);
        this.dismissLoading();
      });
  }

  activateWS() {
    this.client.activate();
  }

  deActivateWS() {
    this.client.deactivate();
  }


  ngOnDestroy() {
    this.deActivateWS();
  }

  configWS() {
    this.presentLoading();
    this.client = new Client();
    this.client.webSocketFactory = () => {
      return new SockJS(this.mUrl + '/mototaxis');
    };

    this.client.onConnect = (frame) => {
      if (this.client.connected) {
        this.dismissLoading();
        console.log('Conectado: ' + this.client.connected);

        this.client.subscribe('/ubicaciones/piloto-on', e => {
          const data = JSON.parse(e.body);
          this.ubicacion = data.body.RES;
          EncryptAndStorage.setEncryptStorage(constantesId.ubicacionPilotoId, this.ubicacion.id);
        });

        this.client.subscribe('/ubicaciones/piloto-off', e => {
          const ubicacion = JSON.parse(e.body);
          console.log(ubicacion.body.RES);
          const id = ubicacion.body.RES.usuario.id;
          this.quitarInactivo(id);
        });

        this.client.subscribe('/ubicaciones/piloto-conectado', e => {
          console.log(JSON.parse(e.body));
          this.pilotos.push(JSON.parse(e.body));
        });


       


      }

    };

    this.client.onDisconnect = (frame) => {
      console.log('Desconectado');
    };
  }

  async abrirChat(piloto: any) {
    console.log(piloto);
    const modal = await this.modalController.create({
      component: ChatComponent,
      componentProps: {
        pilot: piloto
      }
    });
    return await modal.present();
  }

  quitarInactivo(id) {
    console.log(id);
    this.pilotos = this.pilotos
      .filter(a =>
        (a.usuario.id !== id)
      );
  }

  async verPerfilPiloto(piloto: any) {
    const modal = await this.modalController.create({
      component: PerfilComponent,
      componentProps: {
        pilot: piloto
      }
    });
    return await modal.present();
  }

  verCercanos() {
    this.pilotosMostrar = [];
    this.pilotos = this.pilotos
      .filter(p =>
        this.calculateDistance(p.lng, this.myLocation.longitud, p.lat, this.myLocation.latitud) < 300
      )

    this.pilotos.forEach(p => {
      p.distancia = this.calculateDistance(p.lng, this.myLocation.longitud, p.lat, this.myLocation.latitud);
      this.pilotosMostrar.push(p);
    });

    if(this.pilotosMostrar.length == 0) {
      this.mensajeMostrarLista = 'No hay mototaxis cercanos'
    } else {
      this.pilotosMostrar.sort(function(a, b){
        return (a.distancia - b.distancia)
      });
    }

  }

  calculateDistance(lon1, lon2, lat1, lat2) {
    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((lat1 - lat2) * p) / 2 + c(lat2 * p) * c((lat1) * p) * (1 - c(((lon1 - lon2) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a)));
    return Math.trunc(dis * 1000);
  }

  async hacerFavorito(id) {
    const alert = await this.alertController.create({
      header: 'Agregar favorito',
      message: '¿Desea <strong>agregar</strong> este usuario a favoritos?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Agregar',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      spinner: 'lines-small',
      cssClass: 'spinner-loading',
      message: 'Conectando...'
    });
    await loading.present();
  }

  private dismissLoading() {
    this.loadingController.dismiss();
  }



  getLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.myLocation.latitud = resp.coords.latitude;
      this.myLocation.longitud = resp.coords.longitude;


    }).catch((error) => {
      console.log(error);
    });
  }



  async solicitarViaje(id) {
    const alert = await this.alertController.create({
      header: 'Soliciitar viaje',
      message: '¿Desea <strong>solicitar</strong> un viaje?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Solicitar',
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }



}




