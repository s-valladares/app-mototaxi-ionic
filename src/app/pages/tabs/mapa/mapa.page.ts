import { Component, OnInit } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  GoogleMapsAnimation,
  MarkerIcon
} from '@ionic-native/google-maps';
import { IUbicacion, Ubicacion } from 'src/app/services/ubicacion/ubicacion.interface';
import { delay } from 'q';
import { debounce, debounceTime } from 'rxjs/operators';
import { AuthService } from 'src/app/services/services.index';
import { IUbicaciones, Ubicaciones } from 'src/app/services/interfaces.index';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-mapa',
  templateUrl: 'mapa.page.html',
  styleUrls: ['mapa.page.scss']
})
export class MapaPage implements OnInit {

  public creado: boolean;
  public parar: boolean;
  public velocidad: number;

  public optionsMarker: MarkerOptions;
  public mapOptions: GoogleMapOptions;

  public map: GoogleMap;

  public ubicacion: IUbicacion;
  public ubicaciones: IUbicacion[];
  public marker: Marker;
  public markers: any[];
  public idMarcador: string;

  public location: IUbicaciones;

  isLoading = false;
  

  constructor(
    private geolocation: Geolocation,
    private service: AuthService,
    private loadingController: LoadingController
  ) {

    this.creado = false;
    this.parar = false;
    this.velocidad = 0;
    this.ubicacion = Ubicacion.empty();
    this.ubicaciones = [];
    this.idMarcador = '';

    this.location = Ubicaciones.empty();
  }

  ngOnInit() {
    // this.watchLocation();
    this.getLocation();
    // this.getAll();
    // this.getAllBlog();
  }

  ionViewDidLoad() {
    // your code;
  }

  getAllBlog() {
    this.service.Blog()
      .then(data => {
        console.log(data);

      }).catch(error => {

        console.log(error);
      });
  }


  getLocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.location.latitud = resp.coords.latitude;
      this.location.longitud = resp.coords.longitude;

      console.log(resp.coords.latitude);
      console.log(resp.coords.longitude);
      this.loadMap();

    }).catch((error) => {
      console.log(error);
    });
  }

  watchLocation() {
    const watch = this.geolocation.watchPosition({
      maximumAge: 3000,
      timeout: 5000,
      enableHighAccuracy: true,
    });

    watch.subscribe((data) => {

      this.ubicacion.lat = data.coords.latitude;
      this.ubicacion.lng = data.coords.longitude;

      this.velocidad = data.coords.speed;

      if (!this.creado) {
        this.loadMap();
      }
      /*
            if (!this.parar) {
              this.agregarMarcador();
            }
      */
    });

  }

  moverMarcador() {

  }

  agregarMarcador(lati, lngi) {

    const icono: MarkerIcon = {
      url: 'assets/icon/t2.png',
      size: {
        width: 32,
        height: 24
      }
    };

    this.map.addMarker({
      position: { lat: lati, lng: lngi },
      icon: icono
   });


    // this.activar();
    /*
        this.map.addMarker(this.optionsMarker)
          .then((marker: Marker) => {
            this.idMarcador = marker.getId();
            alert(this.idMarcador);
          }, error => {
            alert(error);
          });
    */

    /*
        if (this.ubicacion.id !== ubicacion.id) {

          this.marker.setPosition({
            lat: this.ubicacion.lat,
            lng: this.ubicacion.lng
          });

        } else {

          this.map.addMarker(options).then((marker: Marker) => {
            this.marker = marker;
            this.ubicacion.marcador = marker.getId();
            this.marker.showInfoWindow();

          });

        }
      */

  }

  loadMap() {
    this.presentLoading();
    this.mapOptions = {
      camera: {
        target: {
          lat: this.location.latitud,
          lng: this.location.longitud
        },
        zoom: 18,
        tilt: 30
      }
    };

    try {
     
      this.map = GoogleMaps.create('mapa', this.mapOptions);
      this.map.one(GoogleMapsEvent.MAP_READY).then(this.onMapReady.bind(this));
    } catch (error) {
      this.dismissLoading();
      console.log('ocrruio')
    }
    
   
  }

  onMapReady() {
    this.dismissLoading();
    this.creado = true;
    this.agregarMarcador(this.location.latitud, this.location.longitud);
  }

 



  insertLocFb() {



    /*
        this.marker = this.agregarMarcador(this.ubicacion.lat, this.ubicacion.lng);
        this.ubicacion.marcador = this.marker.getId();
    
        this.firestoreService.create(this.ubicacion)
          .then(ubicacion => this.ubicacion.id = ubicacion.id)
          .catch(error => alert(error));
    */
  }



  async presentLoading() {

    this.isLoading = true;
    return await this.loadingController.create(
      {
        spinner: 'lines-small',
        cssClass: 'spinner-loading',
        message: 'Cargando mapa...'
      }
    ).then(a => {
      a.present().then(() => {
        if (!this.isLoading) {
          a.dismiss();
        }
      });
    });

  }

  async dismissLoading() {
    if (this.isLoading) {
      this.isLoading = false;
      return await this.loadingController.dismiss();
    }
    return null;
  }

}
