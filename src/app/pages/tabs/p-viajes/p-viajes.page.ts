import { Component, OnInit } from '@angular/core';
import { constantesId } from 'src/app/services/misc/enums';
import { EncryptAndStorage } from 'src/app/services/misc/storage';
import { PilotosService } from 'src/app/services/services.index';

@Component({
  selector: 'app-p-viajes',
  templateUrl: './p-viajes.page.html',
  styleUrls: ['./p-viajes.page.scss'],
})
export class PViajesPage implements OnInit {

  mensajeMostrarLista: string;
  viajes: any[];
  pilotoId;
  urlAvatar: string;

  constructor(
    private service: PilotosService
  ) {
    this.mensajeMostrarLista = 'No hay viajes pendientes';
    this.viajes = [];
    this.pilotoId = '';
    
   }

  ngOnInit() {
    this.pilotoId = EncryptAndStorage.getEncryptStorage(constantesId.pilotoId);
    this.urlAvatar = '../../../../assets/icon/usuario.svg';
  }

  segmentChanged(ev: any) {
    console.log('Segmento activo: ', ev.detail.value);
    if (ev.detail.value == 'pendientes') {
      this.mensajeMostrarLista = 'No hay viajes pendientes';
    } else{
      this.getAllViajes();
      this.mensajeMostrarLista = '';
    } 
  }

  getAllViajes() {

    this.service.getAllViajesPilotoId(this.pilotoId)
      .then(data => {
        this.viajes = data.rows;
        console.log(this.viajes);
      })
      .catch(error => {
        console.log(error);
      });
  }

  verPerfilUsuario(item){

  }

}
