import { Component, OnInit, Input } from "@angular/core";
import { LoadingController, ModalController } from "@ionic/angular";
import { Client } from "@stomp/stompjs";
import * as SockJS from "sockjs-client";
import { ConfigService } from "src/app/services/config/config.service";
import { IPilotos, Usuario } from "src/app/services/interfaces.index";
import {
  IMensajes,
  Mensajes,
} from "src/app/services/Mensajes/mensaje.interface";
import { MensajeService } from 'src/app/services/Mensajes/mensaje.service';
import { constantesId } from "src/app/services/misc/enums";
import { EncryptAndStorage } from "src/app/services/misc/storage";
import Swal from "sweetalert2";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit {
  @Input() pilot: IPilotos;
  urlAvatar: string;
  mensajes: IMensajes[];
  mensaje: IMensajes;
  texto: string;
  usuarioId: string;

  private client: Client;

  private mUrl = this.configService.urlWebSocket;

  constructor(
    public modalController: ModalController,
    private loadingController: LoadingController,
    private configService: ConfigService,
    private mensajeServicio: MensajeService
  ) {
    this.mensajes = [];
    this.mensaje = Mensajes.empty();
    this.texto = "";
    
  }

  ngOnInit() {
    console.log(this.pilot);
    this.usuarioId = EncryptAndStorage.getEncryptStorage(
      constantesId.usuarioId
    );
    this.urlAvatar = "../../../../assets/icon/usuario.svg";
    this.configWS();
  }

  cerrarChat() {
    this.modalController.dismiss();
  }

  enviarMensaje() {
    this.mensaje.mensaje = this.texto;
    this.mensaje.usuarioEnvia.id = this.usuarioId;
    this.mensaje.usuarioRecibe.id = this.pilot.usuario.id;
    //this.sendMessageWS();
    this.enviarMensajeFCM();
  }

  enviarMensajeFCM(){
    const msj = {
      title: 'Nuevo mensaje',
      message: this.texto,
      click_action: 'FCM_PLUGIN_ACTIVITY',
      token: this.pilot.usuario.tokenFCM
    }

    this.mensajeServicio.newMensajeTest(msj)
    .then(respnse => {
      console.log(respnse);
    })
    .catch(error => {
      console.log(error);
    })
  }

  configWS() {
    this.presentLoading();
    this.client = new Client();
    this.client.webSocketFactory = () => {
      return new SockJS(this.mUrl + "/mototaxis");
    };

    this.client.onConnect = (frame) => {
      if (this.client.connected) {
        this.dismissLoading();
        console.log("Conectado: " + this.client.connected);

        this.client.subscribe("/ubicaciones/mensajes", (e) => {
          const respuesta = JSON.parse(e.body).body;
          

          if (respuesta.mensaje !== "OK") {
            this.alertError("Ocurrió un error", respuesta.mensaje);
          } else {

            this.mensaje = respuesta.RES;

            if(this.mensaje.usuarioEnvia.id === this.usuarioId){
              this.mensaje.type = 'send';
            } else {
              this.mensaje.type = 'received';
            }
           
            this.mensajes.push(this.mensaje);
            this.mensaje = Mensajes.empty();
          }
        });
      }
    };

    this.client.onDisconnect = (frame) => {
      console.log("Desconectado");
    };

    this.client.activate();
  }

  sendMessageWS() {
    this.client.publish({
      destination: "/api/mensajes",
      body: JSON.stringify(this.mensaje),
    });
    this.texto = "";
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      spinner: "lines-small",
      cssClass: "spinner-loading",
      message: "Cargando configuraciones...",
    });
    await loading.present();
  }

  private dismissLoading() {
    this.loadingController.dismiss();
  }

  alertError(mensaje, foot) {
    Swal.fire({
      icon: "error",
      title: "¡Error!",
      text: mensaje,
      footer: foot,
    });
  }
}
