import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ChatComponent } from './chat/chat.component';

@Component({
  selector: 'app-mensajes',
  templateUrl: './mensajes.page.html'
})
export class MensajesPage implements OnInit {

  urlAvatar: string;
  idUsuario: string;


  constructor(
    public modalController: ModalController
  ) {
    this.idUsuario = '';
   }

  ngOnInit() {
    this.urlAvatar = '../../../../assets/icon/usuario.svg';
  }

  async abrirChat(dd: any) {
    const piloto = {
      id: 1,
      usuario: {
        id: 1,
        persona: {nombres: 'Salvador', apellidos: 'Valladares'}
      }
      
    }
    const modal = await this.modalController.create({
      component: ChatComponent,
      componentProps: {
        pilot: piloto
      }
    });
    return await modal.present();
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: ChatComponent,
      componentProps: {
        idUsuario: this.idUsuario
      }
    });
    return await modal.present();
  }

}
