import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss'],
})
export class PerfilComponent implements OnInit {


  @Input() pilot: any;
  urlAvatar = "../../../../assets/icon/usuario.svg";

  constructor(
    private modal: ModalController
  ) { }

  ngOnInit() {}

  cerrarChat() {
    this.modal.dismiss();
  }


}
