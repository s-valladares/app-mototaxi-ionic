import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoginGuard } from './services/guards/login.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('../app/pages/Usuario/login/login.module').then(m => m.LoginPageModule),
    canActivate: [LoginGuard]
  },
  {
    path: 'home',
    loadChildren: () => import('../app/pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'p-viajes',
    loadChildren: () => import('./pages/tabs/p-viajes/p-viajes.module').then( m => m.PViajesPageModule)
  },  {
    path: 'u-viajes',
    loadChildren: () => import('./pages/Usuario/u-viajes/u-viajes.module').then( m => m.UViajesPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
